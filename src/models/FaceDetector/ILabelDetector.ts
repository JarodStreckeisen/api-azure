export default interface ILabelDetector {

  MakeAnalysisRequest(imageFilePath: string): void;

  // tslint:disable-next-line: unified-signatures
  MakeAnalysisRequest(bucketName: string, dataObjectName: string): void;

  toString(): string;
}