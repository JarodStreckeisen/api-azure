import axios from "axios";
import dotenv from "dotenv";
dotenv.config();
import fs from "fs-extra"
import * as path from "path";
import ILabelDetector from "./ILabelDetector";

export default class AzureFaceDetector implements ILabelDetector {

  private subscriptionKey: string;
  private uriBase: string;
  private contentString: string;

  constructor() {
    this.uriBase = process.env.URI_BASE;
    this.subscriptionKey = process.env.AZURE_KEY;
  }
  /**
   * This method is designed to Get the analysis of the specified local image by using the Face REST API.
   * @param imageFilePath
   * @returns Axios response
   */
  public async MakeAnalysisRequest(imageFilePath: string) {

    // Request parameters. A third optional parameter is "details".
    const params = "returnFaceId=false&returnFaceLandmarks=false" +
      "&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses," +
      "emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";

    // options required for Azure
    const headers = {
      'Ocp-Apim-Subscription-Key': this.subscriptionKey,
      'Content-Type': 'application/octet-stream'
    };

    const imageBuffer = fs.readFileSync(path.resolve(__dirname, imageFilePath));
    const uri = `${this.uriBase}?${params}`;

    return await new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: uri,
        // tslint:disable-next-line: object-literal-shorthand
        headers: headers,
        data: imageBuffer
      }).then((res) => {
        this.contentString = res.data
        resolve(this.contentString);
      }).catch((err) => {
        reject(err.response.data.error);
      })
    })
  }

  /**
   * This method returns, in json format, all labels collected
   * @returns json
   */
  public toString = (): string => {
    return JSON.stringify(this.contentString, null, '  ');
  }

}