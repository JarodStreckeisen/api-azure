import AzureFaceDetector from './models/FaceDetector/AzureFaceDetector'
const jsonFile = require("../images/expected.json");
const expectedJson = JSON.stringify(jsonFile, null, '  ');

test('Test if the JSON is correct', async () => {
    const azure = new AzureFaceDetector();
    azure.MakeAnalysisRequest("../../../images/gandhi.jpg")
    // tslint:disable no-console
    // tslint:disable-next-line: no-shadowed-variable
    .then((response) => {
        expect(azure.toString()).toBe(expectedJson)
    })
    // tslint:disable-next-line: no-console
    .catch((error) => console.log(error))
})