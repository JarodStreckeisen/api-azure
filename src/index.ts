import express, { response } from "express";
import isEqual from "lodash/isEqual"
const app = express();
const port = 8080; // default port to listen
import AzureFaceDetector from './models/FaceDetector/AzureFaceDetector'

// tslint:disable-next-line:no-var-requires
const jsonFile = require("../images/expected.json");
const expectedJson = JSON.stringify(jsonFile, null, '  ');

// tslint:disable-next-line:no-console
console.log(expectedJson)

// define a route handler for the default home page
app.get( "/api/*", async ( req, res) => {

	// tslint:disable-next-line:no-console
	console.log(req.params[0])
	res.send(req.params[0])
});

// start the Express server
app.listen(port, () => {
	// tslint:disable-next-line:no-console
	console.log(`server started at http://localhost:${port}`);
});