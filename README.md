Server which was developed in Node.js with TypeScript compiler and is using the Face API from Azure.

# Pre-reqs

To build and run this app locally you will need a few things:

- Install [Node.js](https://nodejs.org/en/)
- Install [VS Code](https://code.visualstudio.com/)
- Install a navigator (Except Edge)

# Getting started

First, you need to create your .env based on the example with your Azure Face API credentials that you can get [here](https://azure.microsoft.com/en-gb/services/cognitive-services/face/).

```
AZURE_KEY = 
URI_BASE = 
```

Then, clone the repository

```
git clone https://Anel_24@bitbucket.org/JarodStreckeisen/api-azure.git
```

Install dependencies

```
cd <project_name>
npm install
```

To start the server

```
npm run start
```

To test our API GET

```
http://localhost:8080/api/<your_image_url>
```

To generate code documentation


```
npm run doc
```

# Debian config 

[Debian config doc](documentation/Debian%20config%20%2B%20deployment.md)
[Infrastructur diagram](documentation/Infrastructure%20diagram.png)

